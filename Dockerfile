FROM debian:buster-slim as mixmaster-deps
RUN apt update -y; apt install -y libwww-perl libmailtools-perl wget && \
  apt install -y dpkg-dev debhelper libncurses5-dev libpcre3-dev && \
  apt install -y libssl-dev zlib1g-dev bison po-debconf && \
  rm -rf /var/lib/apt/lists/*

FROM mixmaster-deps as mixmaster
COPY patches patches
# Change wget and create local dirs instead
RUN wget https://archive.raspbian.org/raspbian/pool/main/m/mixmaster/mixmaster_3.0.0.orig.tar.gz && \
  wget https://archive.raspbian.org/raspbian/pool/main/m/mixmaster/mixmaster_3.0.0-10.debian.tar.xz && \
  tar xvzf mixmaster_3.0.0.orig.tar.gz && \
  tar xvf mixmaster_3.0.0-10.debian.tar.xz && \
  mv patches/* debian/etc_mixmaster/ && \
  mv debian mixmaster-3.0 && \
  cd mixmaster-3.0 && \
  dpkg-buildpackage && \
  cd .. && \
  dpkg -i mixmaster_3.0.0-10_amd64.deb && \
  rm -rf mixmaster* patches


RUN adduser --shell /bin/bash --disabled-password --gecos "" mix
RUN chown -R mix. /home/mix
USER mix

CMD ["mixmaster", "-h"]
